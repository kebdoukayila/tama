// Créer une classe Barre de vie
class BarreDeVie {
  _vie;
  _couleur;
  // Si on nous passe les paramètres vie et couleur, on les utilise, sinon on prend les paramètres par défaut.
  // On vérifie aussi que vie est comprise entre 0 et 100.
  constructor(vie = 100,couleur = "green"){
    if (vie <= 100 && vie >= 0) {
      this._vie = vie;
      this._couleur = couleur;
    }else{
      throw('la vie doit être comprise entre 0 et 100')
    }

    let container = document.querySelector('#container');
    let barredevie = document.querySelector('#barredevie');
    container.style.border = "2px solid black";
    creationBarre(this._vie,this._couleur);
  }

  get vie(){
    return this._vie;
  }


  set gagnerVie(vie){
    if (this._vie + vie <= 100) {
      this._vie += vie;
    }else{
      this._vie = 100;
    }
    creationBarre(this._vie,this._couleur)

  }




set perdreVie(vie){
    if (this._vie - vie >= 0) {
      this._vie -= vie;
    }else{
      this._vie = 0;
    }
    creationBarre(this._vie,this._couleur)

  }







}

// Lorsqu'on l'appelle, ça crée un élément sur la page html
//
// avoir plusieurs méthodes pour :
// - gagner de la vie
// - perdre de la vie
//
// Dans le cas où la barre arrive à 0, afficher "tu es mort".
//


function creationBarre(vie,couleur){
  container.innerHTML = "<div id=\"barredevie\" style=\"background-color:"+couleur+";width:"+vie+"%; height:20px; text-align:center; color:#FFF;\"></div>"
  barredevie.innerHTML = vie+"%";
}


let barre = new BarreDeVie;
